# **Folding Covid Isolation Box**
The Covid Isolation Box is a folded clear plastic enclosure designed to protect practitioners from sputum and other bodily fluids emitted by covid-19 patients. The design is lightweight and disposable, and can be stacked at pre-staging locations around an emergency department for quick deployment. The box is roughly 22" tall, 24" wide, and 20" deep when deployed, providing plenty of room around the patient's head and upper torso for intubation and other procedures (note that the first image below is an earlier iteration with less headroom). On-site assembly takes less than five minutes, and the box uses ~$15 in materials for construction.

## status
The Covid Isolation Box is currently undergoing IRB-approved trials at Boston Medical Center with 35 commercially sourced units. Feedback from medical professionals involved in this trial will feed in to future design iterations.

<img src="img/MIT Box 2-min.jpg" alt="Smiley face" width="800" >

<img src="img/rotul.png" alt="Smiley face" width="800" >

Thanks to Marty Culpepper and the Project Manus group for design consultations and suggestions, particularly related to the curved/angled design and the snap-fit connection.

## assembly instructions
The Covid Isolation Box ships flat. In the following diagram, the black lines are cut at the factory, while the green lines are pre-creased:

![foldmap](img/foldmap.png)

The machine-made creases are quite accurate but need to be re-folded so the box is square and sturdy after assembly. All of the re-fold operations happen the same direction (i.e., they are all "mountain folds" when viewed from outside the assembled box). Perform these operations by carefully closing the folds between your thumb and forefinger and tightly squeezing the plastic together, as if you were folding a piece of paper or card stock. First, re-fold all of the straight green lines other than those marked with an "X":

![instructions1](img/foldmap_instructions1.png)

Next, re-fold the two curved creases. This is a bit more fussy since you can't just fold the whole line all at once; work your way down from one side to the other, making sure to follow the crease line:

![instructions2](img/foldmap_instructions2.png)

That's it for re-folding. Now, start assembling the box by inserting tabs into slots. First, connect the two pillar reinforcements:

![instructions3](img/foldmap_instructions3.png)

Then insert the patient arch connectors into the frame with the arch on the outside, noting that the snap from the previous step fits into a slot on the arch:

![instructions4](img/foldmap_instructions4.png)

Next, insert the doctor access panel connectors into the frame with the panel on the outside:

![instructions5](img/foldmap_instructions5.png)

Next, connect the two flaps below the doctor access panel. Note that the connector in the middle goes through one flap and then the access panel; if the material binds, reverse the stacking order:

![instructions6](img/foldmap_instructions6.png)

Finally, connect the four flaps to open the doctor access hatch:

![instructions7](img/foldmap_instructions7.png)


# ***Chronological Process***
## _04/08/20_

Based on the [feedback](feedback.md) that the doctors gave us on the last version of the [intubation box](https://gitlab.cba.mit.edu/zfredin/intubation-station) Zach and I made, I decided to start from scratch with a total different approach to this intubation box.

<img src="img/foldedboxV2.jpeg" alt="Smiley face" height="560" >

It is proposed a folding kirigami isolation box. Points that we are solving immediately form the feedback are:

  - Completely transparent
  - No sharp edges
  - There are no worrying gaps

Added extra interesting features are:
  - From a 4 x 8 ft sheet of PETG two isolation boxes can be made
  - Cutting on the Zund in 3.5 min per box
  - Flat storage
  - Protective plastic that helps on deliver non-contaminated devices

On the other hand, _**there are two aspects we have to iterate on this design**. One is **it is more than 20 seconds assembly**_ and the other one is that _**on the patient side of the box, the structural integrity can be improved**_. We will work on that points towards.

<img src="img/structural_integrity.gif" alt="Smiley face" height="500" >

For the time being, this is de 2d design and the folded device.

<img src="img/cutted_shape.jpeg" alt="Smiley face" height="560" >
<img src="img/corner_detail.jpeg" alt="Smiley face" height="560" >

Mock-up has been made with a 4 by 8 ft PETG sheet 0.5mm thickness. Cut on the Zund with the EOT1 and z22 blade, 100mm linear velocity and the creasing tool at 500 mm/min.



## _04/09/20_

After a bunch of iterations and some really inspiring meetings, today we came up with a new version of the mock-up that solves a lot of the problems listed yesterday.

<img src="img/transparentv8.jpeg" alt="Smiley face" height="560" >


Same photo now but without removing the protective film.
<img src="img/opaquev8.jpeg" alt="Smiley face" height="500" >

I has been added curved creased lateral hinges due to the bi-stability that provides, compared like adding a chamfer. The total time of cutting the element is 2min 10 seconds with the cutting conditon as z22 blade at 400 mm/min and creasing tool at 1000mm/min. Also, two of this model fits on a 4 by 8 ft PETG sheet.

<img src="img/v8dxf.png" alt="Smiley face" height="560" >

The folding time has been increased due to all the improvements in the geometry that has been made. Here you can find all the iterations done today. (the actual V9 is the left transparent guy. Its so difficult to take photos to this transparent sheets!)

<img src="img/all_versions.jpeg" alt="Smiley face" height="560" >


Excited to know feedback from the doctors tomorrow but looking good.



## _04/11/20_
## **Feedback**

Doctors contacted Zach and gave the feedback of this new approach. The main points they told us were:

>Shape and fold design is great. Boxes stacked well, **we feel comfortable with the plan to fold ~20 in advance** and replenish folded stock by a trained folder as they are depleted. Having 3 sides contact the foundation surface worked well.

>**Stability was good**, no significant deformation during intubation attempts.

>**Edges were all smooth**, no cuts, no sharp corners. Great job on this.

>No big gaps in the folds/joints. **This box was as close to airtight** as we need to get. Excellent.

>**Height is an issue**, it definitely needs to be taller (see pictures below). Given the flexibility of this material, 24-25" wide might actually work. We got this one to sit nicely around the cushion and I think it was 24"? **If we can make a version that is 22” tall, 24” wide, and about the same depth (from patient’s head to feet), we would probably be golden**.

>**Two use cases** for disposable boxes:

>**Intubation:** Use to protect laryngoscopist during procedure, then dispose.

>**Long-term aerosol protection:** Place over intubated patients in the ICU to protect staff in the event the patient regains consciousness and self-extubates. For this use, we might be able to get away with smaller dimensions to get more out of the stock. We should test one at 24” wide and see how it goes.

<img src="img/MIT Box 1-min.jpg" alt="Smiley face" height="560" >
<img src="img/MIT Box 2-min.jpg" alt="Smiley face" height="560" >




Based on the following feedback, the next step is to make a taller box. It is a challenge due to we are making a folding structure. If we want to keep continuity and increase the height, that means we have to grow in both normal directions. As we are already using the max width (4 ft), we have to fold more complex shapes to fill the gap we will generate in the front.

## _04/13/20_

After some cad, seems like we have a likely solution to be offered tomorrow. At 9am doctors are coming to pick two samples and test them again, so new feedback is coming.

Here at your right is the new model.
<img src="img/comp.jpeg" alt="Smiley face" height="560" >

As it can be seen, it simnifically bigger. **22 inches! :)** We have increase 8 in from the last model.

<img src="img/smheigh.jpeg" alt="Smiley face" height="560" >


<img src="img/tall.jpeg" alt="Smiley face" height="560" >

Not because being small has less structural integrity. Triangular folding beams placed in the rear arch made this guy **stand up to 10 pounds**

<img src="img/weight.jpeg" alt="Smiley face" height="560" >

The front closing, which as commented before, it was a challenge, has been solved making the front part of the box to work in tension with the commented beams and adding clip joints.
<img src="img/frontdetaill.jpeg" alt="Smiley face" height="560" >


Nothing much more to add now, looking forward to see the doctor opinion.
